<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = ['nickname', 'gender', 'morning', 'noon', 'night'];

    public function getMorningVideoAttribute()
    {
        return Moment::find($this->morning);
    }
    public function getNoonVideoAttribute()
    {
        return Moment::find($this->noon);
    }
    public function getNightVideoAttribute()
    {
        return Moment::find($this->night);
    }
}
