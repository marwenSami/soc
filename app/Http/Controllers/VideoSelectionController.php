<?php

namespace App\Http\Controllers;

use App\Moment;
use App\Nickname;
use App\User;
use App\Video;
use App\VideoRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class VideoSelectionController extends Controller
{

    public function chooseNickname(Request $request)
    {
        // Auth check

        $nicknames = Nickname::all();
        return view('form.part1.show')->with('nicknames', $nicknames);
    }

    public function getFormPart3($id)
    {
        return view('form.part3.show')->with("video_id", $id);
    }
    public function submitFormPart3(Request $request, $id)
    {
        // Verif Form results
        $this->validate($request, [
            'name' => 'required|max:255',
            'cin' => 'required|digits:8',
            'email' => 'required|email',
            'tel' => 'required|digits:8',
        ]);
        $u = User::find(Auth::id());
        $u->name = $request->name;
        $u->cin = $request->cin;
        $u->email = $request->email;
        $u->tel = $request->tel;
        $u->save();
        return redirect()->route('video_preview', $id);
    }

    public function chooseMoment(Request $request)
    {

        $nicknames = Nickname::all();
        $array_of_nicknames = $nicknames->pluck('value')->all();
        $this->validate($request, [
            'nickname' => [
                'required',
                Rule::in($array_of_nicknames)
            ],
            'gender' => [
                'required',
                Rule::in(['male', 'female'])
            ],
        ]);

        $vr = new VideoRequest();
        //$vr->user()->associate(Auth::user());
        $vr->nickname = $request->nickname;
        $vr->gender = $request->gender;

        $request->session()->put('video_request', $vr);

        $morning = Moment::whereCat('morning')->get();
        $noon = Moment::whereCat('noon')->get();
        $night = Moment::whereCat('night')->get();
        return view('form.part2.show')->with(["morning" => $morning, "noon" => $noon, "night" => $night]);
    }

    public function getVideo(Request $request)
    {
        // Auth check

        $vr = $request->session()->get('video_request');

        if(is_null($vr)){
            return redirect()->route('nickname_show');
        }

        // Process Form 3 results
        $vr->morning = $request->morning;
        $vr->noon = $request->noon;
        $vr->night = $request->night;

        $vr->save();

        $request->session()->forget('video_request');

        // Fetch the video
        $video = Video::whereNickname($vr->nickname)->whereGender($vr->gender)->whereMorning($vr->morning)->whereNoon($vr->noon)->whereNight($vr->night)->first();
        if(is_null($video))
            abort(404, "La video demandé n'est pas disponible");

        $request->session()->put('video_id', $video->id);

        return redirect()->route('video_show', $video->id);
    }

    public function showVideo($id){
        $video = Video::find($id);
        return view('video.show')->with('video', $video);
    }

    public function getVideoRequest()
    {
        // Auth check

        $vr = VideoRequest::all();
        return view('vr.index')->with('vr', $vr);
    }

    public function getVideoPreview($id)
    {
        $video = Video::find($id);
        return view('video.preview')->with('video', $video);
    }
}
