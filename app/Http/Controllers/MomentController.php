<?php

namespace App\Http\Controllers;

use App\Moment;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MomentController extends Controller
{

    // Validate forms


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Moment::orderBy('cat')->get();
        return view('moments.index', ["videos" => $videos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('moments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(is_null($request->file('video')))
            new \Exception("You should select a file");
        $video = Moment::firstOrNew(
            [
                'cat' => $request->cat,
                'index' => $request->index
            ]
        );
        $video->name = $request->name;
        $video->path = $request->file('video')->store('moments');
        $video->save();

        return redirect()->route('moments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $m = Moment::find($id);
        $delete = true;
        $error = "";
        switch ($m->cat){
            case "morning":
                if(count(Video::whereMorning($id)->get())>0){
                    $delete = false;
                    $error = "This moment is affected to a video";
                }
                break;
            case "noon":
                if(count(Video::whereNoon($id)->get())>0){
                    $delete = false;
                    $error = "This moment is affected to a video";
                }
                break;
            case "night":
                if(count(Video::whereNight($id)->get())>0){
                    $delete = false;
                    $error = "This moment is affected to a video";
                }
                break;
        }
        if($delete) {
            Storage::delete($m->path);
            $m->delete();
        }
        return redirect()->route('moments.index')->withErrors(["error" => $error]);
    }

    public function show_moment($mPath)
    {
        $path = storage_path() . '/app/' . $mPath;

        if (file_exists($path)) {
            $type = File::mimeType($path);

            return response(Storage::get($mPath), 200, ['Content-Type' => $type]);
        }

        return response()->json(['Status Code' => '404 Not Found'], 404);
    }
}
