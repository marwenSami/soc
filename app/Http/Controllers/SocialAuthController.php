<?php

namespace App\Http\Controllers;

use App\Providers\SocialServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Socialite;

class SocialAuthController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function callback(SocialServiceProvider $service, Request $request)
    {
        $s = Socialite::driver('facebook')->user();
        $user = $service->createOrGetUser($s);
        Auth::login($user);
        $id = $request->session()->get('video_id');
        return redirect()->route('winform_show', $id);
    }
}
