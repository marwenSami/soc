<?php

namespace App\Http\Controllers;

use App\Moment;
use App\Nickname;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::all();
        return view('videos.index', ["videos" => $videos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $nicknames = Nickname::all();
        $morning = Moment::whereCat('morning')->get();
        $noon = Moment::whereCat('noon')->get();
        $night = Moment::whereCat('night')->get();
        return view('videos.create')->with(["nicknames" => $nicknames, "morning" => $morning, "noon" => $noon, "night" => $night]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(is_null($request->file('video')))
            new \Exception("You should select a file");
        $video = Video::firstOrNew(
            [
                'nickname' => $request->nickname,
                'gender' => $request->gender,
                'morning' => $request->morning,
                'noon' => $request->noon,
                'night' => $request->night
            ]
        );
        $video->path = $request->file('video')->store('videos');
        $video->save();

        return redirect()->route('videos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('videos.show')->with('video', Video::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $v = Video::find($id);
        Storage::delete($v->path);
        $v->delete();
        return redirect()->route('videos.index');
    }
}
