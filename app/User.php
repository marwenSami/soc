<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    const ROLE_USER = 1;
    const ROLE_MODERATOR = 2;
    const ROLE_ADMINISTRATOR = 3;
    const ROLE_SUPER_ADMINISTRATOR = 4;

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'name', 'email', 'password', 'avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getRoleNameAttribute()
    {
        switch ($this->role){
            case User::ROLE_USER:
                return "User";
                break;
            case User::ROLE_MODERATOR:
                return "Moderator";
                break;
            case User::ROLE_ADMINISTRATOR:
                return "Administrator";
                break;
            default:
                return "Super Administrator";
        }
    }

    public function social()
    {
        return $this->hasOne(SocialAccount::class);
    }
}
