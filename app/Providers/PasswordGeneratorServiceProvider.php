<?php
/**
 * Created by PhpStorm.
 * User: marwen
 * Date: 7/19/17
 * Time: 1:55 PM
 */

namespace App\Providers;


class PasswordGeneratorServiceProvider
{
    public function generate($len=12)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $len; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}