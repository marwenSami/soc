<?php
/**
 * Created by PhpStorm.
 * User: marwen
 * Date: 7/19/17
 * Time: 11:52 AM
 */

namespace App\Providers;

use App\SocialAccount;
use App\User;
use Laravel\Socialite\Contracts\User as ProviderUser;
use App\Providers\PasswordGeneratorServiceProvider;

class SocialServiceProvider
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $password = new PasswordGeneratorServiceProvider();
        $account = SocialAccount::whereProviderUserId($providerUser->getId())->first();
        if ($account) {
            return $account->user;
        } else {

            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId()
            ]);

            $user = User::whereUsername($providerUser->getId())->first();

            if (!$user) {
                $email = (is_null($providerUser->getEmail()))?'unknown':$providerUser->getEmail();

                $user = User::create([
                    'username' => $providerUser->getId(),
                    'email' => $email,
                    'password' => bcrypt($password->generate()),
                    'name' => $providerUser->getName(),
                    'avatar' => $providerUser->getAvatar(),
                ]);
            }


            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }
}