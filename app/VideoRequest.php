<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoRequest extends Model
{
    protected $table = 'video_request';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getMorningVideoAttribute()
    {
        return Moment::find($this->morning);
    }
    public function getNoonVideoAttribute()
    {
        return Moment::find($this->noon);
    }
    public function getNightVideoAttribute()
    {
        return Moment::find($this->night);
    }

}
