@extends('layouts.dashboard')

@section('content')
        <div class="col-md-12">
            <h2>Administrators:</h2>
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>E-mail</th>
                    <th>Role</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    @if($user->role > \App\User::ROLE_USER)
                    <tr>
                        <th scope="row">{{$user->id}}</th>
                        <td>{{$user->name}}</td>
                        <td>{{$user->username}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            @if($user->id != Auth::user()->id)
                            <form method="POST" action="{{route("users.update", $user->id)}}">
                                {{csrf_field()}}
                                {{ method_field('PUT')}}
                                <select name="role">
                                    <option value="{{$user->role}}">{{$user->role_name}}</option>
                                    <option value="1">User</option>
                                    <option value="2">Moderator</option>
                                    <option value="3">Administrator</option>
                                    <option value="4">Super Administrator</option>
                                </select>
                                <button type="submit" class="btn btn-success">Send</button>
                            </form>
                            @endif
                        </td>
                        <td>
                            <form method="POST" action="{{route("users.destroy", $user->id)}}">
                                {{csrf_field()}}
                                {{ method_field('DELETE')}}
                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
            <h2>Clients:</h2>
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>E-mail</th>
                    <th>CIN</th>
                    <th>Phone number</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    @if($user->role < \App\User::ROLE_MODERATOR)
                    <tr>
                        <th scope="row">{{$user->id}}</th>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->cin}}</td>
                        <td>{{$user->tel}}</td>
                        <td>
                            <form method="POST" action="{{route("users.destroy", $user->id)}}">
                                {{csrf_field()}}
                                {{ method_field('DELETE')}}
                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
@endsection