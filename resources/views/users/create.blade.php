@extends('layouts.dashboard')

@section('content')
    <form class="col" action="{{route('users.store')}}" method="post">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {{csrf_field()}}
        <div class="form-group row">
            <label class="col-2 col-form-label" for="name">Name</label>
            <div class="col-10">
                <input type="text" id="name" name="name" class="form-control" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 col-form-label" for="username">Username</label>
            <div class="col-10">
                <input type="text" id="username" name="username" class="form-control" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-2 col-form-label">E-mail</label>
            <div class="col-10">
                <input type="email" id="email" name="email" class="form-control" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="password" class="col-2 col-form-label">Password</label>
            <div class="col-10">
                <input type="password" id="password" name="password" class="form-control" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="password_confirmation" class="col-2 col-form-label">Confirm password</label>
            <div class="col-10">
                <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="role" class="col-2 col-form-label">Role</label>
            <div class="col-10">
                <select name="role" id="role" class="form-control" required>
                    <option value="{{\App\User::ROLE_MODERATOR}}">Moderator</option>
                    <option value="{{\App\User::ROLE_ADMINISTRATOR}}">Administrator</option>
                    <option value="{{\App\User::ROLE_SUPER_ADMINISTRATOR}}">Super Administrator</option>
                </select>
            </div>
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
    </form>
@endsection