@extends('layouts.dashboard')

@section('content')
    <form class="col" action="{{route('users.update', $user->id)}}" method="post">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {{csrf_field()}}
        {{ method_field('PUT')}}
        <div class="form-group row">
            <label class="col-2 col-form-label" for="name">Name</label>
            <div class="col-10">
                <input type="text" id="name" name="name" value="{{$user->name}}" class="form-control" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 col-form-label" for="username">Username</label>
            <div class="col-10">
                {{$user->username}}
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-2 col-form-label">E-mail</label>
            <div class="col-10">
                {{$user->email}}
            </div>
        </div>
        <div class="form-group row">
            <label for="old_password" class="col-2 col-form-label">Old Password</label>
            <div class="col-10">
                <input type="password" id="old_password" name="old_password" class="form-control" required>
            </div>
        </div>
        <div class="row"><h2>Change Password</h2></div>
        <div class="form-group row">
            <label for="password" class="col-2 col-form-label">Password</label>
            <div class="col-10">
                <input type="password" id="password" name="password" class="form-control">
            </div>
        </div>
        <div class="form-group row">
            <label for="password_confirmation" class="col-2 col-form-label">Confirm password</label>
            <div class="col-10">
                <input type="password" id="password_confirmation" name="password_confirmation" class="form-control">
            </div>
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
    </form>
@endsection