@extends('layouts.dashboard')

@section('content')
    <form class="col" action="{{route('nicknames.store')}}" method="post">
        {{csrf_field()}}
        <div class="form-group row">
            <label for="value" class="col-2 col-form-label">Value</label>
            <div class="col-10">
                <input type="text" class="form-control" name="value" id="value" required>
            </div>
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
    </form>
@endsection