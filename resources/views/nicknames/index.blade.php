@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Value</th>
                    <th>Delete</th>
                </tr>
                </thead>
                <tbody>
                @foreach($nicknames as $nickname)
                    <tr>
                        <th scope="row">{{$nickname->id}}</th>
                        <td>{{$nickname->value}}</td>
                        <td>
                            <form method="POST" action="{{route("nicknames.destroy", $nickname->id)}}">
                                {{csrf_field()}}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection