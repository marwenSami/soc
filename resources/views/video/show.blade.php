@extends('layouts.base')
@section('custom-css')
    <style>
        body{
            background:url('{{asset('img/background3.jpg')}}') no-repeat center center fixed;
            height: 100%;
            padding-bottom: 0;
        }
    </style>
@endsection
@section('content')
    <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#"></a>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item fa fa-file-text mr-4">
                    <a class="nav-link" href="#"></a>
                </li>
                <li class="nav-item fa fa-volume-up  mr-4">
                    <a class="nav-link" href="#"></a>
                </li>
                <li class="nav-item fa fa-th">
                    <a class="nav-link" href="#"></a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container-fluid mt-4">
        <div class="row">
            <div class="col-md-6 offset-md-6">
                <video class="rounded" width="100%" controls id="video1">
                    <source src="{{Storage::url($video->path)}}" type="video/mp4">
                </video>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 offset-md-6 col-sm-6 text-center">
                <a href="/redirect"><button class="btn btn-danger btn-sharing pr-2 pb-0"><i class="fa fa-gift"></i> Participez et gagnez pleins de cadeaux</button></a>
            </div>
        </div>
    </div>

    <footer class="footer fixed-bottom">
        <div class="container">
            <div class="col-md-10 offset-md-1">
                <div class="row">
                    <div class="col text-center">
                        <p style="color: #ffffff;font-size: 9px">© 2016 The Coca-Cola Company, all rights reserved. Coca-Cola®, "Open Happiness", and the Contour Bottle are registered trademarks of The Coca-Cola Company.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col text-center col-sm-12">
                        <a href="#" class="col-md-1" style="font-size: 9px">Termes et conditions</a>
                        <a href="#" class="col-md-1" style="font-size: 9px">Conditions d'utilisation</a>
                        <a href="#" class="col-md-1" style="font-size: 9px">Politique de confidentialité</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div id="fb-root"></div>
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="https://use.fontawesome.com/81c7fbb76b.js"></script>

    <script>
        $(document).ready(function() {
            $.ajaxSetup({ cache: true });
            $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
                FB.init({
                    appId: '{{config('services.facebook.client_id')}}',
                    version: 'v2.10' // or v2.1, v2.2, v2.3, ...
                });
                FB.getLoginStatus(updateStatusCallback);
            });

            document.getElementById('btn-share').onclick = function() {
                FB.ui({
                    method: 'share',
                    mobile_iframe: true,
                    href: '{{route('preview_show', $video->id)}}',
                }, function(response){});
            };
        });

    </script>
@endsection
