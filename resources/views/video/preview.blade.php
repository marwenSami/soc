@extends('layouts.base')
@section('custom-css')
    <style>
        body{
            background:url('{{asset('img/background3.jpg')}}') no-repeat center center fixed;
            height: 100%;
            padding-bottom: 0;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid mt-4">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <video class="rounded" width="100%" controls id="video1">
                    <source src="{{Storage::url($video->path)}}" type="video/mp4">
                </video>
            </div>
        </div>
    </div>

    <footer class="footer fixed-bottom">
        <div class="container">
            <div class="col-md-10 offset-md-1">
                <div class="row">
                    <div class="col text-center">
                        <p style="color: #ffffff;font-size: 9px">© 2016 The Coca-Cola Company, all rights reserved. Coca-Cola®, "Open Happiness", and the Contour Bottle are registered trademarks of The Coca-Cola Company.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col text-center col-sm-12">
                        <a href="#" class="col-md-1" style="font-size: 9px">Termes et conditions</a>
                        <a href="#" class="col-md-1" style="font-size: 9px">Conditions d'utilisation</a>
                        <a href="#" class="col-md-1" style="font-size: 9px">Politique de confidentialité</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="https://use.fontawesome.com/81c7fbb76b.js"></script>
@endsection
