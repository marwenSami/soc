@extends('layouts.base')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-md-center text-center">
        <div class="col-md-3 connexion rounded">
            <h2 class="connextion_title mb-5 mt-4">Connexion</h2>
            <div class="fb-login-button mb-5" data-size="medium" data-max-rows="1">
                <a href="/redirect"><img class="w-100" src="{{asset('img/connect.png')}}" alt="connect with Facebook"></a>
            </div>
        </div>

    </div>
</div>
<footer class="footer fixed-bottom">
    <div class="col-md-10 offset-md-1">
        <div class="row">
            <div class="col text-center">
                <p style="color: #ffffff">© 2016 The Coca-Cola Company, all rights reserved. Coca-Cola®, "Open Happiness", and the Contour Bottle are registered trademarks of The Coca-Cola Company.</p>
            </div>
        </div>
        <div class="row">
            <div class="col text-center col-sm-12">
                <a href="#" class="col-md-1">Termes et conditions</a>
                <a href="#" class="col-md-1">Conditions d'utilisation</a>
                <a href="#" class="col-md-1">Politique de confidentialité</a>
            </div>
        </div>
    </div>
</footer>
@endsection