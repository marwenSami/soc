@extends('layouts.dashboard')

@section('content')
    <div class="col">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            @foreach($videos as $video)
            <div class="col-4">
                <div class="card">
                    <video width="320" height="240" controls>
                        <source src="{{Storage::url($video->path)}}" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                    <div class="card-block">
                        <h4 class="card-title">
                            {{$video->name}} <span class="badge badge-pill badge-info pull-right">{{ucfirst($video->cat)}} {{$video->index}}</span>
                        </h4>
                        <div class="card-text text-center">
                            <div class="btn-group">
                                <form action="{{route("moments.destroy", $video->id)}}" method="post">
                                    {{csrf_field()}}
                                    {{method_field("DELETE")}}
                                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">Created at: {{$video->created_at}} | Updated at: {{$video->updated_at}}</small>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@endsection