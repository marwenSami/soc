@extends('layouts.dashboard')

@section('content')
    <form class="col" action="{{route('moments.store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group row">
            <label for="name" class="col-2 col-form-label">Video name</label>
            <div class="col-10">
                <input type="text" class="form-control" name="name" id="name" required>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-2 col-form-label" for="video">Video file</label>
            <div class="col-10">
                <input type="file" id="video" name="video">
            </div>
        </div>
        <div class="form-group row">
            <label for="cat" class="col-2 col-form-label">Category</label>
            <div class="col-10">
                <select name="cat" id="cat"  class="form-control" required>
                    <option value="morning">Morning</option>
                    <option value="noon">Noon</option>
                    <option value="night">Night</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="index" class="col-2 col-form-label">Index</label>
            <div class="col-10">
                <select name="index" id="index"  class="form-control" required>
                    <option value="1">1</option>
                    <option value="2">2</option>
                </select>
            </div>
        </div>
        <button type="submit" class="btn btn-success">Valider</button>
    </form>
@endsection