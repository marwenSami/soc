@extends('layouts.base')
@section('custom-css')
    <style>
    body{
        background:url('{{asset('img/background3.jpg')}}') no-repeat center center fixed;
        height: 100%;
        padding-bottom: 0;
    }
    </style>
@endsection
@section('content')
    <nav class="navbar navbar-toggleable-md navbar-light bg-faded">

        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#"></a>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item fa fa-file-text mr-4">
                    <a class="nav-link" href="#"></a>
                </li>
                <li class="nav-item fa fa-volume-up  mr-4">
                    <a class="nav-link" href="#"></a>
                </li>
                <li class="nav-item fa fa-th">
                    <a class="nav-link" href="#"></a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container-fluid mt-2">
        <div class="row">
            <div class="col-md-6 offset-md-6 col-sm-10 offset-sm-1">
                <div class="row">
                    <div class="offset-md-1 col-md-10 mb-4 col-sm-10 offset-sm-1">
                        <h2 class="title3">إختار تفرشيكتك</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div class="row">
                            @foreach($morning as $m)
                            <div class="col-md-6 step-container-1">
                            <video class="rounded video-step-1" width="100%" autoplay loop data-value="{{$m->id}}" data-src="{{Storage::url($m->path)}}">
                                <source src="{{Storage::url($m->path)}}" type="video/mp4">
                            </video>
                            </div>
                            @endforeach
                            @foreach($noon as $m)
                                <div class="col-md-6 step-container-2" style="display:none;">
                                    <video class="rounded video-step-2" width="100%" autoplay loop data-value="{{$m->id}}" data-src="{{Storage::url($m->path)}}">
                                        <source src="{{Storage::url($m->path)}}" type="video/mp4">
                                    </video>
                                </div>
                            @endforeach
                            @foreach($night as $m)
                                <div class="col-md-6 step-container-3" style="display:none;">
                                    <video class="rounded video-step-3" width="100%" autoplay loop data-value="{{$m->id}}" data-src="{{Storage::url($m->path)}}">
                                        <source src="{{Storage::url($m->path)}}" type="video/mp4">
                                    </video>
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="offset-md-1 col-md-10 mb-4 col-sm-10 offset-sm-1 text-center mt-3">

                        <div class="btn step step-1 step-selected rounded">1</div>
                        <div class="btn step step-2 rounded">2</div>
                        <div class="btn step step-3 rounded">3</div>

                    </div>
                </div>

            </div>
        </div>


        <div class="row mt-5">
            <div class="col-md-10 offset-md-1">
                <div class="row">
                    <div class="col-md-2 col-sm-2">
                        <button class="btn btn-ps btn-danger col-sm-10 offset-sm-1 pull-right"  id="precedent">Précédent</button>
                    </div>

                    <div class="regle hidden-sm-down col-md-8">
                        <div class="row timeline-thumb">
                            <div class="col-md-4 text-center thumbnail thumbnail-step-1">
                                <video class="rounded thumbnail-video-1" width="100%" autoplay loop>
                                    <source class="thumbnail-src-1" src="" type="video/mp4">
                                </video>
                            </div>

                            <div class="col-md-4 text-center thumbnail thumbnail-step-2">
                                <video class="rounded thumbnail-video-2" width="100%" autoplay loop>
                                    <source class="thumbnail-src-2" src="" type="video/mp4">
                                </video>
                            </div>

                            <div class="col-md-4 text-center thumbnail thumbnail-step-3">
                                <video class="rounded thumbnail-video-3" width="100%" autoplay loop>
                                    <source class="thumbnail-src-3" src="" type="video/mp4">
                                </video>
                            </div>

                        </div>

                    </div>
                    <div class="col-md-2 col-sm-2">
                        <button class="btn btn-ps col-sm-10 offset-sm-1 btn-danger" id="suivant">Suivant</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <footer class="footer fixed-bottom">
        <div class="container">
            <div class="col-md-10 offset-md-1">
                <div class="row">
                    <div class="col text-center">
                        <p style="color: #ffffff;font-size: 9px">© 2016 The Coca-Cola Company, all rights reserved. Coca-Cola®, "Open Happiness", and the Contour Bottle are registered trademarks of The Coca-Cola Company.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col text-center col-sm-12">
                        <a href="#" class="col-md-1" style="font-size: 9px">Termes et conditions</a>
                        <a href="#" class="col-md-1" style="font-size: 9px">Conditions d'utilisation</a>
                        <a href="#" class="col-md-1" style="font-size: 9px">Politique de confidentialité</a>
                    </div>
                </div>
            </div>
            </div>

    </footer>


    <form action="{{route('video_get')}}" method="POST" id="moment-form" style="display: none;">
        {{csrf_field()}}
        <input type="input" name="morning" id="input-morning" value="">
        <input type="input" name="noon" id="input-noon" value="">
        <input type="input" name="night" id="input-night" value="">
    </form>
    <script src="js/jquery.js"></script>
    <script>
        $(function() {
            var current_step = 1;
            $('#precedent').addClass("disabled");
            $('#suivant').addClass("disabled");
            $('.thumbnail').hide();
            $('.step-container-2').hide();
            $('.step-container-3').hide();

            $('#suivant').click(function () {
                if(!$(this).hasClass("disabled")) {
                    if(current_step ==3){
                        $('.thumbnail-step-' + current_step).show();
                        document.getElementById('moment-form').submit();
                    }else {
                        $('.step').removeClass('step-selected');

                        $('.step-container-' + current_step).hide();
                        $('.thumbnail-step-' + current_step).show();

                        current_step++;
                        $('.step-' + current_step).addClass('step-selected');
                        $('.step-container-' + current_step).show();

                        $(this).addClass("disabled");
                        $('#precedent').removeClass("disabled");
                    }
                }
            });

            $('#precedent').click(function () {
                if(!$(this).hasClass("disabled")) {
                    $('.step').removeClass('step-selected');

                    $('.step-container-'+current_step).hide();

                    current_step--;
                    $('.step-' + current_step).addClass('step-selected');
                    $('.step-container-'+current_step).show();

                    if(current_step == 1)
                        $(this).addClass("disabled");
                    $('#suivant').removeClass("disabled");
                }
            });

            $('.video-step-1').click(function () {

                $('.video-step-1').removeClass('video-selected');
                $(this).addClass('video-selected');

                var video_id = $(this).data('value');
                $("#input-morning").val(video_id);

                var video_src = $(this).data('src');
                $(".thumbnail-src-1").attr('src', video_src);
                $(".thumbnail-video-1").load();
                $('#suivant').removeClass("disabled");
            });

            $('.video-step-2').click(function () {

                $('.video-step-2').removeClass('video-selected');
                $(this).addClass('video-selected');

                var video_id = $(this).data('value');
                $("#input-noon").val(video_id);

                var video_src = $(this).data('src');
                $(".thumbnail-src-2").attr('src', video_src);
                $(".thumbnail-video-2").load();
                $('#suivant').removeClass("disabled");
            });

            $('.video-step-3').click(function () {

                $('.video-step-3').removeClass('video-selected');
                $(this).addClass('video-selected');

                var video_id = $(this).data('value');
                $("#input-night").val(video_id);

                var video_src = $(this).data('src');
                $(".thumbnail-src-3").attr('src', video_src);
                $(".thumbnail-video-3").load();

                $('#suivant').removeClass("disabled");
            });

        });
    </script>
    <script src="https://use.fontawesome.com/81c7fbb76b.js"></script>
@endsection
