@extends('layouts.base')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger alert-fxd">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><small>{{ $error }}</small></li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container-fluid mb-5">
        <div class="row justify-content-md-center text-center">
            <div class="col-md-4 vc p-5">

                <div class="title">كوّن تفرشيكتك</div>
                <form action="{{route('moment_show')}}" method="POST">
                    {{csrf_field()}}

                    <input list="nicknames" class="form-control custom-select" name="nickname" id="nickname" placeholder="Akhtar tarbijtek" required>
                    <datalist id="nicknames">
                        @foreach($nicknames as $nickname)
                            <option value="{{$nickname->value}}">
                        @endforeach
                    </datalist>
                    <input id="male" type="radio" name="gender" value="male">
                    <label for="male">Homme</label>
                    <input id="female" type="radio" name="gender" value="female">
                    <label for="female">Femme</label>

                    <button type="submit" class="btn-continue btn btn-block">Continuer</button>
                </form>
            </div>
        </div>
    </div>
    <footer class="footer fixed-bottom">
        <div class="container">
            <div class="col-md-10 offset-md-1">
                <div class="row">
                    <div class="col text-center">
                        <p style="color: #ffffff;font-size: 9px">© 2016 The Coca-Cola Company, all rights reserved. Coca-Cola®, "Open Happiness", and the Contour Bottle are registered trademarks of The Coca-Cola Company.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col text-center col-sm-12">
                        <a href="#" class="col-md-1" style="font-size: 9px">Termes et conditions</a>
                        <a href="#" class="col-md-1" style="font-size: 9px">Conditions d'utilisation</a>
                        <a href="#" class="col-md-1" style="font-size: 9px">Politique de confidentialité</a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
@endsection
