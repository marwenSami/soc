@extends('layouts.base')
@section('content')
    @if ($errors->any())
        <div class="alert alert-danger alert-fxd">
            <ul>
                @foreach ($errors->all() as $error)
                    <li><small>{{ $error }}</small></li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container-fluid">
        <div class="row justify-content-md-center text-center">
            <div class="col-md-4 vc5">
                <div class="title5">Wooow</div>
                <div class="sub-title5">! vidéo <span class="arabic">ملا</span></div>
                <form action="{{route('winform_send', $video_id)}}" method="POST">
                    {{csrf_field()}}
                    <p id="msg">Pour valider votre inscription, et entrer dans le tirage au sort,remplissez le formulaire</p>
                    <div class="row">
                        <div class="col-md-10 offset-md-1 pb-3">
                            <input type="text" class="rounded form-control input-formulaire" placeholder="Nom et Prénom*" name="name" value="{{ Auth::user()->name }}" required/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10 offset-md-1 pb-3">
                            <input type="email" class="rounded form-control input-formulaire" placeholder="E-mail*" name="email" value="{{ Auth::user()->email }}" required />
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-5 offset-md-1">
                            <input type="number" class="rounded form-control input-formulaire" placeholder="CIN*" name="cin" required/>
                        </div>
                        <div class="col-md-5 ">
                            <input type="tel" class="rounded form-control input-formulaire" placeholder="Tel*" name="tel" required/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 offset-md-1">
                            <p class="identite">carte d'indentité nationale</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10 offset-md-1 mt-1 mb-2">
                            <button type="submit" class="btn btn-inscription btn-block" id="submit">S'inscrire</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 offset-md-3 ">
                            <p class="ps">En créant un compte. vous continuez que vous acceptez les conditions d'utilisation
                                et la politique de confidentialité.</p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <footer class="text-center mt-4">
        <div class="col-md-10 offset-md-1">

            <div class="row">
                <div class="col text-center">
                    <p style="color: #ffffff ;font-size: 9px">© 2016 The Coca-Cola Company, all rights reserved. Coca-Cola®, "Open Happiness", and the Contour Bottle are registered trademarks of The Coca-Cola Company.</p>
                </div>
            </div>

            <div class="row">
                <div class="col text-center col-sm-12">
                    <a href="#" class="col-md-1" style="font-size: 9px">Termes et conditions</a>
                    <a href="#" class="col-md-1"  style="font-size: 9px">Conditions d'utilisation</a>
                    <a href="#" class="col-md-1"  style="font-size: 9px">Politique de confidentialité</a>
                </div>
            </div>
        </div>

    </footer>

    <div id="fb-root"></div>
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="https://use.fontawesome.com/81c7fbb76b.js"></script>

    <script>
        $(document).ready(function() {
            $.ajaxSetup({ cache: true });
            $.getScript('//connect.facebook.net/en_US/sdk.js', function(){
                FB.init({
                    appId: '{{config('services.facebook.client_id')}}',
                    version: 'v2.10' // or v2.1, v2.2, v2.3, ...
                });
                FB.getLoginStatus(updateStatusCallback);
            });

            document.getElementById('submit').onclick = function() {
                FB.ui({
                    method: 'share',
                    mobile_iframe: true,
                    href: '{{route('preview_show', $video_id)}}',
                }, function(response){});
            };
        });

    </script>

@endsection