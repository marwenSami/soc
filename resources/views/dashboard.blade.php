@extends('layouts.dashboard')

@section('content')
    <!-- Icon Cards -->
    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card card-inverse card-primary o-hidden h-100">
            <div class="card-block">
                <div class="card-block-icon">
                    <i class="fa fa-fw fa-play-circle"></i>
                </div>
                <div class="mr-5">
                    26 Video generated
                </div>
            </div>
            <a href="#" class="card-footer clearfix small z-1">
                <span class="float-left">View Details</span>
                <span class="float-right"><i class="fa fa-angle-right"></i></span>
            </a>
        </div>
    </div>
    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card card-inverse card-success o-hidden h-100">
            <div class="card-block">
                <div class="card-block-icon">
                    <i class="fa fa-fw fa-tasks"></i>
                </div>
                <div class="mr-5">
                    11 Request in process
                </div>
            </div>
            <a href="#" class="card-footer clearfix small z-1">
                <span class="float-left">View Details</span>
                <span class="float-right"><i class="fa fa-angle-right"></i></span>
            </a>
        </div>
    </div>
    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card card-inverse card-warning o-hidden h-100">
            <div class="card-block">
                <div class="card-block-icon">
                    <i class="fa fa-fw fa-shopping-cart"></i>
                </div>
                <div class="mr-5">
                    123 New Orders!
                </div>
            </div>
            <a href="#" class="card-footer clearfix small z-1">
                <span class="float-left">View Details</span>
                <span class="float-right"><i class="fa fa-angle-right"></i></span>
            </a>
        </div>
    </div>
    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card card-inverse card-danger o-hidden h-100">
            <div class="card-block">
                <div class="card-block-icon">
                    <i class="fa fa-fw fa-support"></i>
                </div>
                <div class="mr-5">
                    13 New Tickets!
                </div>
            </div>
            <a href="#" class="card-footer clearfix small z-1">
                <span class="float-left">View Details</span>
                <span class="float-right"><i class="fa fa-angle-right"></i></span>
            </a>
        </div>
    </div>
@endsection
