@extends('layouts.dashboard')

@section('content')
    <div class="col-md-12">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>User</th>
                <th>Nickname</th>
                <th>Gender</th>
                <th>Morning</th>
                <th>Noon</th>
                <th>Night</th>
            </tr>
            </thead>
            <tbody>
            @foreach($vr as $v)
                <tr>
                    <th scope="row">{{$v->id}}</th>
                    <td>{{$v->user->name}} <small>(ID:{{$v->user->id}})</small></td>
                    <td>{{$v->nickname}}</td>
                    <td>{{$v->gender}}</td>
                    <td>{{$v->morning_video->name}}</td>
                    <td>{{$v->noon_video->name}}</td>
                    <td>{{$v->night_video->name}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection