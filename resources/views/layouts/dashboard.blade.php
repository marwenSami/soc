<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, user-scalable=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Share a Coke') }}</title>

    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap core CSS -->
    <link href="{{asset("vendor/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset('css/sb-admin.css')}}" rel="stylesheet">

</head>
<body id="page-top">
<!-- Navigation -->
<nav id="mainNav" class="navbar static-top navbar-toggleable-md navbar-inverse bg-inverse">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarExample" aria-controls="navbarExample" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="{{route('home')}}">{{config('app.name')}} Dashboard</a>
    <!-- /.navbar-header -->
    <div class="collapse navbar-collapse" id="navbarExample">
        <ul class="sidebar-nav navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="{{route('home')}}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li class="nav-item">
                <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMoments"><i class="fa fa-video-camera fa-fw"></i> Moments<span class="fa arrow"></span></a>
                <ul class="sidebar-second-level collapse" id="collapseMoments">
                    <li>
                        <a href="{{route('moments.index')}}">Moments list</a>
                    </li>
                    <li>
                        <a href="{{route('moments.create')}}">Manage moments</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li class="nav-item">
                <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents"><i class="fa fa-video-camera fa-fw"></i> Videos<span class="fa arrow"></span></a>
                <ul class="sidebar-second-level collapse" id="collapseComponents">
                    <li>
                        <a href="{{route('videos.index')}}">Videos list</a>
                    </li>
                    <li>
                        <a href="{{route('videos.create')}}">Add a new video</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li class="nav-item">
                <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseNicknames"><i class="fa fa-vcard fa-fw"></i> Nicknames<span class="fa arrow"></span></a>
                <ul class="sidebar-second-level collapse" id="collapseNicknames">
                    <li>
                        <a href="{{route('nicknames.index')}}">Nicknames list</a>
                    </li>
                    <li>
                        <a href="{{route('nicknames.create')}}">Add a new nickname</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseUsers"><i class="fa fa-user fa-fw"></i> Users<span class="fa arrow"></span></a>
                <ul class="sidebar-second-level collapse" id="collapseUsers">
                    <li>
                        <a href="{{route('users.index')}}">Users list</a>
                    </li>
                    <li>
                        <a href="{{route('users.create')}}">Create a new user</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('video_request_show')}}"><i class="fa fa-bars fa-fw"></i> Video Requests</a>
            </li>
        </ul>
        <!-- /.sidebar-collapse -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{route('users.edit', Auth::user()->id)}}"><i class="fa fa-fw fa-user"></i> My Profile</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="fa fa-fw fa-sign-out"></i> Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </div>
</nav>
<div class="content-wrapper py-3">
    <div class="container-fluid">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active">My Dashboard</li>
        </ol>
        <div class="row">
            @yield('content')
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript -->
<script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('vendor/tether/tether.min.js')}}"></script>
<script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>

<!-- Plugin JavaScript -->
<script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
<script src="{{asset('vendor/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>

<!-- Custom scripts for this template -->
<script src="{{asset('js/sb-admin.min.js')}}"></script>
</body>
</html>
