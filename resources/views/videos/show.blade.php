@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <video width="320" height="240" controls>
                    <source src="{{$video->path}}" type="video/mp4">
                    Your browser does not support the video tag.
                </video>                <div class="card-block">
                    <h4 class="card-title">
                        {{$video->name}} <span class="badge badge-pill badge-info">@if($video->watermark) Contains a nickname @endif</span>
                    </h4>
                    <div class="card-text text-center">
                        <div class="btn-group">
                            <a href="{{route('videos.show', $video->id)}}" class="btn btn-success"><i class="fa fa-eye"> Watch</i></a>
                            <button class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</button>
                            <button class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <small class="text-muted">Created at: {{$video->created_at}} | Updated at: {{$video->updated_at}}</small>
                </div>
            </div>
        </div>
    </div>
@endsection