@extends('layouts.dashboard')

@section('content')
    <div class="col">
        <div class="row">
            @foreach($videos as $video)
                <div class="col-4">
                    <div class="card">
                        <video width="320" height="240" controls>
                            <source src="{{Storage::url($video->path)}}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                        <h4 class="card-title text-center">
                            <span class="badge badge-pill badge-info">{{ucfirst($video->nickname)}}</span>
                            <span class="badge badge-pill badge-warning">{{ucfirst($video->gender)}}</span>
                        </h4>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">{{$video->morning_video->name}}</li>
                            <li class="list-group-item">{{$video->noon_video->name}}</li>
                            <li class="list-group-item">{{$video->night_video->name}}</li>
                        </ul>
                        <div class="card-block">
                            <div class="card-text text-center">
                                <div class="btn-group">
                                    <form action="{{route("videos.destroy", $video->id)}}" method="post">
                                        {{csrf_field()}}
                                        {{method_field("DELETE")}}
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Created at: {{$video->created_at}} | Updated at: {{$video->updated_at}}</small>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection