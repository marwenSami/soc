@extends('layouts.dashboard')

@section('content')
    <form class="col" action="{{route('videos.store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group row">
            <label class="col-2 col-form-label" for="video">Video file</label>
            <div class="col-10">
                <input type="file" id="video" name="video" required>
            </div>
        </div>
        <div class="form-group row">
            <label for="nickname" class="col-2 col-form-label">Nickname</label>
            <div class="col-10">
                <select name="nickname" id="nickname" class="form-control" required>
                    @foreach($nicknames as $nickname)
                        <option value="{{$nickname->value}}">{{$nickname->value}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="gender" class="col-2 col-form-label">Gender</label>
            <div class="col-10">
                <select name="gender" id="gender" class="form-control" required>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="morning" class="col-2 col-form-label">Morning</label>
            <div class="col-10">
                <select name="morning" id="morning" class="form-control" required>
                    @foreach($morning as $m)
                        <option value="{{$m->id}}">{{$m->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="noon" class="col-2 col-form-label">Noon</label>
            <div class="col-10">
                <select name="noon" id="noon" class="form-control" required>
                    @foreach($noon as $m)
                        <option value="{{$m->id}}">{{$m->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="night" class="col-2 col-form-label">Night</label>
            <div class="col-10">
                <select name="night" id="night" class="form-control" required>
                    @foreach($night as $m)
                        <option value="{{$m->id}}">{{$m->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
    </form>
@endsection