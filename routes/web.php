<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', 'VideoSelectionController@chooseNickname')->name('nickname_show');

Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');
Route::get('/form', 'VideoSelectionController@getFormPart1')->name('form_show');
Route::post('/form', 'VideoSelectionController@getFormPart2')->name('form_send');
Route::get('/win/{id}', 'VideoSelectionController@getFormPart3')->name('winform_show');
Route::post('/win/{id}', 'VideoSelectionController@submitFormPart3')->name('winform_send');
Route::post('/moment', 'VideoSelectionController@chooseMoment')->name('moment_show');
Route::post('/video', 'VideoSelectionController@getVideo')->name('video_get');
Route::get('/video/{id}', 'VideoSelectionController@showVideo')->name('video_show');
Route::get('/video_request', 'VideoSelectionController@getVideoRequest')->name('video_request_show');
Route::get('/preview/{id}', 'VideoSelectionController@getVideoPreview')->name('preview_show');
Auth::routes();

Route::get('/admin', 'HomeController@index')->name('home');

Route::resource('/videos', 'VideoController');
Route::resource('/moments', 'MomentController');
Route::resource('/nicknames', 'NicknameController');
Route::resource('/users', 'UserController');
