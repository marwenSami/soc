<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NicknamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nicknames = ["Marou", "Toutou", "Loulou", "Boff", "Mimi", "Sonson", "Skan"];

        foreach ($nicknames as $nickname) {
            DB::table('nicknames')->insert([
                'value' => $nickname,
            ]);
        }
    }
}
